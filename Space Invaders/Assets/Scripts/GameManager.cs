using UnityEngine;
using UnityEngine.UI;

public sealed class GameManager : MonoBehaviour
{
    [SerializeField] private PlayerController player;    
    [SerializeField] private InvaderGroup invaders;
    [SerializeField] private Invader invader;
    [SerializeField] private Bunker bunker;
    [SerializeField] private PausePopupController pausePopup;
    [SerializeField] private GameOverPopupController gameOverPopup;

    private void Initialize()
    {
        
    }
}