using UnityEngine;
using System;

public class Projectile : MonoBehaviour
{
    public Vector3 direction;

    public float speed;

    public Action destroyed;

    private void Update()
    {
        transform.position += direction * speed * Time.deltaTime;
    }

    private void OnTriggerEnter2D(Collider2D target)
    {
        destroyed?.Invoke();

        Destroy(gameObject);
    }
}
