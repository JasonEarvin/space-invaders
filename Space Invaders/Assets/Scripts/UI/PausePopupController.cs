using UnityEngine;
using UnityEngine.UI;
using System;

public class PausePopupController : MonoBehaviour
{
    [SerializeField] private Canvas canvas;

    [SerializeField] private Button continueButton;
    [SerializeField] private Button resetButton;
    [SerializeField] private Button quitButton;

    public event Action ContinuePressed;
    public event Action ResetPressed;
    public event Action QuitPressed;

    private void Start()
    {
        
    }

    private void OnDestroy()
    {
        
    }

    private void OnContinuePressed()
    {
        Hide();
        ContinuePressed?.Invoke();
    }

    private void OnResetPressed()
    {
        Hide();
        ResetPressed?.Invoke();
    }

    private void OnQuitPressed()
    {
        Hide();
        QuitPressed?.Invoke();
    }

    public void Show()
    {
        canvas.enabled = true;
    }

    public void Hide()
    {
        canvas.enabled = false;
    }
}
